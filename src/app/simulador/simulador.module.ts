import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SimuladorComponent } from './simulador/simulador.component';
import { AposentadoriaComponent } from './aposentadoria/aposentadoria.component';
import { FuturoComponent } from './futuro/futuro.component';
import { InvestimentoComponent } from './investimento/investimento.component';



@NgModule({
  declarations: [SimuladorComponent, AposentadoriaComponent, FuturoComponent, InvestimentoComponent],
  imports: [
    CommonModule
  ]
})
export class SimuladorModule { }
