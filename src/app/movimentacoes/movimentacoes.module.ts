import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MovimentacoesComponent } from './movimentacoes/movimentacoes.component';



@NgModule({
  declarations: [MovimentacoesComponent],
  imports: [
    CommonModule
  ]
})
export class MovimentacoesModule { }
