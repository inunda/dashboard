import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AtendimentoComponent } from './atendimento/atendimento.component';



@NgModule({
  declarations: [AtendimentoComponent],
  imports: [
    CommonModule
  ]
})
export class AtendimentoModule { }
