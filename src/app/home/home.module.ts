import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageComponent } from './home-page/home-page.component';

import { routes } from "./home.routing";
import { RouterModule } from "@angular/router";

@NgModule({
  imports: [RouterModule.forChild(routes), CommonModule],
  exports: [],
  declarations: [HomePageComponent]
})
export class HomeModule { }
