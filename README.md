# Estrutura
___

## componentes:
*Sub categorias dos componentes são items ou podem ser outros componentes.*

* Header
    * Texto: links
    * Opções do usuário ***Componente***
    * Notificações ***Componente***
    * Botão -- 'INVISTA MAIS':
    Realizar um aporte, Aumentar contribuição mensal,
    Fazer uma portabilidade ***Componente***
    * Certificado ***Componente***

## Módulos:
*Sub categorias dos módulos são items ou podem ser outros componentes.*

* Home
    * Botões: Login, Adesão
    * Imagem <2-1>
    * Formulário <2-2>


* Dashbord
    * Header: Titulo, Sub titulo, Saldo total, Total acumulado e Contrib. Mensal <1>
    * Gráfico <2-1> ***Componente***
    * Simulador <2-2 ***Componente***
    * Cards <3-1>
    * Ultimas movimentações <3-2>


* Movimentações
    * Titulo
    * Botões: Data, Tipo de contribuição e ordenação
    * Botão -- 'EMITIR EXTRATO'
    * Tabela ***Componente***


* Formas de Pagamento
    * Titulo com texto <1-1>
    * Minhas contas <1-2>
    * Botões: Débito em contas Corrente, Boleto Bancário, Folha de pagamento <2-1>
    * Texto <2-2>


* Simulador
    * Texto: Titulo e sub titulo <1>
    * Cards: Aposentadoria, Projeto Futuro e investimento <2>


* Atendimento
    * Formulário de contato <1>
    * accordion -- 'FAQ'

___


## Rotas:
```
HOME
│    
│
└─── DASHBOARD
│   │
│   └─── MOVIMENTAÇÕES
│   │
│   └─── FORMAS DE PAGAMENTO
│   │
│   └─── SIMULADOR
│   │   │
│   │   └─── APOSENTADORIA
│   │   │
│   │   └─── PROJETO FUTURO
│   │   │
│   │   └─── investimento
│   │
│   └─── ATENDIMENTO
│
└─── LOGIN
│
└─── ADESÃO
│
└─── CADASTRE-SE GRÁTIS
```
___


## Frameworks e bibliotecas:
* Angular 8+
* Node 10+
* Bootstrap
* Imask
* Chart
* Karma
___
